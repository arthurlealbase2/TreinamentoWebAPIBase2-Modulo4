# Fundamentos API Rest - Testes automatizados


Nesse treinamento, aprendemos a realizar testes automatizados utilizando a ferramenta Postman

<div id="postman-div" align="center">
    <a href="https://www.postman.com/">
    <img src="https://blog.bsource.com.br/assets/img/POSTMAN.png" width="400px" height="200px"/></a>
</div>


## Exportando collections do Postman

1º Passo:

<img src="/ReadMeArquivos/1-passo.png"/>



2º Passo:

<img src="/ReadMeArquivos/2-passo.png"/>



## Executando os testes com Newman

Executar o seguinte comando:

```
newman run Treinamento.postman_collection.json -e ArquivoConfig.postman_environment.json
```

A execução gera o seguinte resultado:

<img src="/ReadMeArquivos/newmanrun.png"/>


## Gerando relatório com Newman

Executar o seguinte comando:

```
newman run Treinamento.postman_collection.json -e ArquivoConfig.postman_environment.json -r html
```

Uma pasta de nome "newman" é criada, ela contém todos os relatórios no formato "html".




